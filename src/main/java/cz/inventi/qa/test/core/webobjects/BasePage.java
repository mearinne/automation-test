package cz.inventi.qa.test.core.webobjects;


import cz.inventi.qa.test.fwmock.objects.WebObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BasePage extends WebObject {
    Menu menu;

    public BasePage(WebDriver driver) {
       super(driver);
       menu = new Menu(driver);
    }

    @FindBy(xpath = "//div[@id='screen-overlay')]")
    WebElement loadingOverlay;
    @FindBy(xpath = "//div[contains(@class, 'articleTitleWrapper')]/h1")
    WebElement articleTitle;

    public Menu getMenu() {
        return menu;
    }

    public WebElement getLoadingOverlay() {
        return loadingOverlay;
    }

    public WebElement getArticleTitle() {
        return articleTitle;
    }
}
