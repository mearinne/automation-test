package cz.inventi.qa.test.core.webobjects;


import cz.inventi.qa.test.fwmock.objects.WebObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class Menu extends WebObject {

    @FindBy(xpath = "//menu/ul/li/a")
    List<WebElement> menuLinks;

    public Menu(WebDriver driver) {
        super(driver);
    }

    public WebElement getMenuLink (String name) {
        for (WebElement menuLink : menuLinks) {
            if (menuLink.getText().toLowerCase().equals(name.toLowerCase())) {
                return menuLink;
            }
        }
        throw new RuntimeException("Menu link not found.");
    }

    public HomePage clickHome () {
        getMenuLink("home").click();
        return new HomePage(getDriver());
    }

    public WhatWeDoPage clickWhatWeDo () {
        getMenuLink("what we do").click();
        return new WhatWeDoPage(getDriver());
    }

    public List<WebElement> getMenuLinks() {
        return menuLinks;
    }
}
