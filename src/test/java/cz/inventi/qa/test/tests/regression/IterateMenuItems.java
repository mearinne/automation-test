package cz.inventi.qa.test.tests.regression;


import cz.inventi.qa.test.tests.core.TestCase;
import org.testng.annotations.Test;

public class IterateMenuItems extends TestCase {

    @Test
    public void iterateMenuItems () {
        homePage
            .getMenu()
            .clickHome()
            .assertPageLoaded()
            .getMenu()
            .clickWhatWeDo()
            .assertPageLoaded();
    }
}
